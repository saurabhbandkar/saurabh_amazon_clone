const dotenv = require('dotenv');
dotenv.config({ path: '../../.env' });

module.exports = {
    "development": {
        "REACT_APP_BASE_URL": process.env.REACT_APP_BASE_URL
    },
    "production": {
        "REACT_APP_BASE_URL": process.env.REACT_APP_BASE_URL
    }
};