const axios = require("axios");
const env = process.env.NODE_ENV || 'development';
const config = require("../config/config.js")[env];
const REACT_APP_BASE_URL = config.REACT_APP_BASE_URL;

let headers = {};

if (localStorage.accessToken) {
    headers.authorization = `Bearer ${localStorage.accessToken}`
}
const axiosInstance = axios.create({
    baseURL: REACT_APP_BASE_URL,
    timeout: 10000,
    headers
});

export default axiosInstance;