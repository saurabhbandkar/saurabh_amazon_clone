import React, { Component } from 'react';
import { GoLocation, GoSearch } from "react-icons/go";
import { GiUsaFlag } from "react-icons/gi";
import Tippy from '@tippy.js/react';
import 'tippy.js/dist/tippy.css';

export default class Headers extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }
    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                    <div className="container-fluid">
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <a className="navbar-brand" href="www">
                                <img src="/assets/images.jpeg" alt="amazon home" width="100" height="50" style={{ "borderRadius": "5px" }} />
                            </a>
                            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                <li className="nav-item">
                                    <a className="nav-link active" aria-current="page" href="www"> <GoLocation /> Deliver to India</a>
                                </li>
                                <div className="form-group mb-3">
                                    <label>Search</label>
                                    <div className="input-group" style={{ "width": "100%" }}>
                                        <div className="dropdown">
                                            <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" >
                                                All
                                            </button>
                                            <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                <li><a className="dropdown-item" href="www">Action</a></li>
                                                <li><a className="dropdown-item" href="www">Another action</a></li>
                                                <li><a className="dropdown-item" href="www">Something else here</a></li>
                                            </ul>
                                        </div>
                                        <input className="form-control"
                                            type={this.state.showPassword ? "text" : "password"}
                                            value={this.state.password}
                                            name="password"
                                            placeholder="Search" />
                                        <div className="input-group-append">
                                            <button className="btn btn-outline-secondary" type="button">
                                                <GoSearch />
                                            </button>
                                        </div>
                                    </div>
                                    <small className="form-text text-danger">{this.state.passwordError}</small>
                                </div>
                                <div>
                                    <Tippy content="Basic Tooltip">
                                        <li className="nav-item">
                                            <button className="btn btn-primary" type="button"><GiUsaFlag /></button>
                                        </li>
                                    </Tippy>
                                </div>




                                <li className="nav-item dropdown">
                                    <a className="nav-link dropdown-toggle" href="www" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        Dropdown
                                    </a>
                                    <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <li><a className="dropdown-item" href="www">Action</a></li>
                                        <li><a className="dropdown-item" href="www">Another action</a></li>
                                        <li><hr className="dropdown-divider" /></li>
                                        <li><a className="dropdown-item" href="www">Something else here</a></li>
                                    </ul>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link disabled" href="www" tabIndex="-1" aria-disabled="className=">Disabled</a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </nav>
                <nav className="navbar navbar-expand-lg navbar-dark" style={{ "background": "#373947" }}>
                    <div className="container-fluid">
                        <button className="btn btn-dark" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample" style={{ "background": "#373947", "borderStyle": "none" }}>
                            All
                        </button>
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent1">
                            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                <li className="nav-item">
                                    <a className="nav-link active" aria-current="page" href="www">Today's Deals</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="www">Customer Service</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="www">Gift Cards</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="www">Registry</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="www">Sell</a>
                                </li>
                            </ul>
                            <div className="nav-item">
                                <a className="nav-link text-light" href="www">Amazon's response to COVID-19</a>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        )
    }
}
