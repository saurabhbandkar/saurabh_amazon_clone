import React, { Component } from 'react';
import './Footer.css';

class Footer extends Component {
    render() {
        return (
            <>

                <div className="footer-start">
                    <hr style={{ "color": "#000" }}></hr>
                    <div>
                        <p>See personalized recommendations</p>
                    </div>
                    <div className="d-grid gap-2 col-2 mx-auto">
                        <button type="button" className="btn btn-warning">Sign In</button>
                    </div>
                    <div>
                        <p>New customer?
                            <li style={{ "display": "inline-block" }}>
                                <a href="/" style={{ "textDecoration": "none" }}>Start here</a>
                            </li>
                        </p>

                    </div>
                    <hr style={{ "color": "#000" }}></hr>
                </div>
                <div className="d-grid ">
                    <button className="btn btn-secondary full" type="button">Back to Top</button>
                </div>
                <div className="Footer-body">
                    <div className="Footer">

                        <ul className="Footer-columns">
                            <li className="mb-3">
                                <h4>
                                    Get to Know Us
                                </h4>
                            </li>
                            <li>
                                <a href="/">Careers</a>
                            </li>
                            <li>
                                <a href="/">Blog</a>
                            </li>
                            <li>
                                <a href="/">About Amazon </a>
                            </li>
                            <li>
                                <a href="/">Investor Relations</a>
                            </li>
                            <li>
                                <a href="/">Amazon Devices</a>
                            </li>
                        </ul>
                        <ul className="Footer-columns">
                            <li className="mb-3">
                                <h4>
                                    Make Money with Us
                                </h4>
                            </li>
                            <li>
                                <a href="/">Sell products on Amazon</a>
                            </li>
                            <li>
                                <a href="/">Sell on Amazon Business</a>
                            </li>
                            <li>
                                <a href="/">Sell apps on Amazon</a>
                            </li>
                            <li>
                                <a href="/">Become an Affiliate</a>
                            </li>
                            <li>
                                <a href="/">Advertise Your Products</a>
                            </li>
                            <li>
                                <a href="/">Self-Publish with Us</a>
                            </li>
                            <li>
                                <a href="/">Host an Amazon Hub</a>
                            </li>
                            <li>
                                <a href="/">See More Make Money with Us</a>
                            </li>

                        </ul>
                        <ul className="Footer-columns">
                            <li className="mb-3">
                                <h4>
                                    Amazon Payment Products
                                </h4>
                            </li>
                            <li>
                                <a href="/">Amazon Business Card</a>
                            </li>
                            <li>
                                <a href="/">Shop with Points</a>
                            </li>
                            <li>
                                <a href="/">Reload Your Balance</a>
                            </li>
                            <li>
                                <a href="/">Amazon Currency Converter</a>
                            </li>
                        </ul>
                        <ul className="Footer-columns">
                            <li className="mb-3">
                                <h4>
                                    Let Us Help You
                                </h4>
                            </li>
                            <li>
                                <a href="/">Amazon and COVID-19</a>
                            </li>
                            <li>
                                <a href="/">Your Account</a>
                            </li>
                            <li>
                                <a href="/">Shipping Rates & Policies</a>
                            </li>
                            <li>
                                <a href="/">Returns & Replacements</a>
                            </li>
                            <li>
                                <a href="/">Manage Your Content and Devices</a>
                            </li>
                            <li>
                                <a href="/">Amazon Assistant</a>
                            </li>
                            <li>
                                <a href="/">Help</a>
                            </li>
                        </ul>
                    </div>

                    <hr style={{ "color": "#fff" }}></hr>
                    <div className="intermediate-footer">
                        <div className="d-grid gap-2 d-md-block">
                            <a className="navbar-brand" href="www">
                                <img src="/assets/images.jpeg" alt="amazon home" width="100" height="50" style={{ "borderRadius": "5px", "marginRight": "2%" }} />
                            </a>
                            <button className="btn change" type="button" >English</button>
                            <button className="btn change" type="button" >$ USD- U.S. Dollar</button>
                            <button className="btn change" type="button" >United States</button>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default Footer;