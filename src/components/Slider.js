import React, { Component } from 'react'
import "./slider.css";

export default class Slider extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }
    render() {
        return (
            <div>
                <div id="carouselExampleControls" className="carousel slide" data-bs-ride="carousel">
                    <div className="carousel-inner">
                        <div className="carousel-item active">
                            <img src={this.props.products[0].image} className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item">
                            <img src={this.props.products[1].image} className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item">
                            <img src={this.props.products[2].image} className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item">
                            <img src={this.props.products[3].image} className="d-block w-100" alt="..." />
                        </div>
                    </div>
                    <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Previous</span>
                    </button>
                    <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
        )
    }
}
