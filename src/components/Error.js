import React from 'react';

export default function Error() {
    return (
        <p>
            Error 500: Error Loading Products Search. Kindly check API URL.
            Please try again after sometime.
        </p>
    );
};