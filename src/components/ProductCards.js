import React, { Component } from 'react';
import "./productCards.css";
import axiosInstance from "../helpers/axiosInstance";
import Error from "./Error";
import Loader from "./Loader";

export default class ProductCards extends Component {
    constructor(props) {
        super(props)
        let category = [];
        this.props.products.data.map((categories) => {
            category.push(categories);
            return true
        })

        this.state =
        {
            isAPIResponseOK: true,
            isProductLoaded: false,
            category,
            categories: [],
            done: false,
            isPageLoadComplete: false
        }
    }

    componentDidMount() {
        this.state.category.map((cat) => {
            axiosInstance
                .get(`/products/category/${cat}`)
                .then(category => {
                    this.state.categories.push(category);
                    this.setState({
                        isProductLoaded: true,
                        isAPIResponseOK: true,

                    });
                    if (this.state.categories.length === 4) {
                        this.setState({
                            done: true,
                            isPageLoadComplete: true
                        });
                    }
                })
                .catch((error) => {
                    console.log("Error loading the products API URL, please try again after sometime");
                    this.setState({
                        isAPIResponseOK: false,
                        isProductLoaded: true,
                        isPageLoadComplete: true
                    });
                });
            return true
        })
    }

    render() {
        return (

            <div>
                {
                    !this.state.isPageLoadComplete && <Loader />
                }
                {
                    !this.state.isAPIResponseOK && <Error />
                }
                {
                    this.state.done &&
                    <div>
                        <div className="createFlex">
                            <div className="card text-left">
                                <div className="card-header">
                                    {this.state.categories[0].data[0].category.toUpperCase()}
                                </div>
                                <div className="card-body">
                                    <p className="card-text">
                                        <img src={this.state.categories[0].data[0].image} className="d-block w-100" alt="..." />
                                    </p>
                                </div>
                                <div className="card-footer text-muted">
                                    <a href="www">See More</a>
                                </div>
                            </div>
                            <div className="card text-left">
                                <div className="card-header">
                                    {this.state.categories[1].data[0].category.toUpperCase()}
                                </div>
                                <div className="card-body">
                                    <p className="card-text">
                                        <img src={this.state.categories[1].data[0].image} className="d-block w-100" alt="..." />
                                    </p>
                                </div>
                                <div className="card-footer text-muted">
                                    <a href="www">Shop Now</a>
                                </div>
                            </div>
                            <div className="card text-left">
                                <div className="card-header">
                                    {this.state.categories[2].data[0].category.toUpperCase()}
                                </div>
                                <div className="card-body">
                                    <p className="card-text">
                                        <img src={this.state.categories[2].data[0].image} className="d-block w-100" alt="..." />
                                    </p>
                                </div>
                                <div className="card-footer text-muted">
                                    <a href="www">See More</a>
                                </div>
                            </div>
                            <div className="card text-left">
                                <div className="card-header">
                                    {this.state.categories[3].data[0].category.toUpperCase()}
                                </div>
                                <div className="card-body">
                                    <p className="card-text">
                                        <img src={this.state.categories[3].data[0].image} className="d-block w-100" alt="..." />
                                    </p>
                                </div>
                                <div className="card-footer text-muted">
                                    <a href="www">Shop Now</a>
                                </div>
                            </div>
                        </div>
                        <div className="createFlex">
                            <div className="card text-left">
                                <div className="card-header">
                                    {this.state.categories[0].data[1].category.toUpperCase()}
                                </div>
                                <div className="card-body">
                                    <p className="card-text">
                                        <img src={this.state.categories[0].data[1].image} className="d-block w-100" alt="..." />
                                    </p>
                                </div>
                                <div className="card-footer text-muted">
                                    <a href="www">See More</a>
                                </div>
                            </div>
                            <div className="card text-left">
                                <div className="card-header">
                                    {this.state.categories[1].data[1].category.toUpperCase()}
                                </div>
                                <div className="card-body">
                                    <p className="card-text">
                                        <img src={this.state.categories[1].data[1].image} className="d-block w-100" alt="..." />
                                    </p>
                                </div>
                                <div className="card-footer text-muted">
                                    <a href="www">Explore</a>
                                </div>
                            </div>
                            <div className="card text-left">
                                <div className="card-header">
                                    {this.state.categories[2].data[1].category.toUpperCase()}
                                </div>
                                <div className="card-body">
                                    <p className="card-text">
                                        <img src={this.state.categories[2].data[1].image} className="d-block w-100" alt="..." />
                                    </p>
                                </div>
                                <div className="card-footer text-muted">
                                    <a href="www">See More</a>
                                </div>
                            </div>
                            <div className="card text-left">
                                <div className="card-header">
                                    {this.state.categories[3].data[1].category.toUpperCase()}
                                </div>
                                <div className="card-body">
                                    <p className="card-text">
                                        <img src={this.state.categories[3].data[1].image} className="d-block w-100" alt="..." />
                                    </p>
                                </div>
                                <div className="card-footer text-muted">
                                    <a href="www">Explore</a>
                                </div>
                            </div>
                        </div>

                    </div>
                }
            </div>
        )
    }
}
