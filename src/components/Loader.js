import React from 'react'

export default function Loader(props) {
    return (
        <div className="overlay d-flex justify-content-center">
            <div className="spinner-border" role="status">
                <span className="sr-only">Loading...</span>
            </div>
        </div>
    )
};