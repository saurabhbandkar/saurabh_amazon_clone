import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Headers from "./components/Headers";
import Footers from "./components/Footers";
import Slider from "./components/Slider";
import Error from "./components/Error";
import Loader from "./components/Loader";
import AllDetails from "./components/AllDetails";
import ProductCards from "./components/ProductCards";
import FootersSecondary from "./components/FootersSecondary";
import axiosInstance from "./helpers/axiosInstance";
import "./App.css";

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoaded: false,
      isAPIResponseOK: true,
      productsList: [],
      isCategoryLoaded: false,
      categoryList: [],
      isPageComplete: false
    }
  }

  componentDidMount() {
    localStorage.clear();
    axiosInstance
      .get("/products")
      .then(product => {
        this.setState({
          productList: product.data,
          isLoaded: true,
          isAPIResponseOK: true,
          isPageComplete: true
        });
      })
      .catch((error) => {
        console.log("Error loading the products API URL, please try again after sometime");
        this.setState({
          isAPIResponseOK: false,
          isCategoryLoaded: true,
          isPageComplete: true
        });
      });

    axiosInstance
      .get("/products/categories")
      .then(category => {
        this.setState({
          categoryList: category,
          isCategoryLoaded: true,
          isAPIResponseOK: true,
        });
      })
      .catch((error) => {
        console.log("Error loading the products API URL, please try again after sometime");
        this.setState({
          isAPIResponseOK: false,
          isCategoryLoaded: true,
        });
      });
  }
  render() {
    return (
      <div className="App">
        {!this.state.isAPIResponseOK && <Error />}
        {this.state.isAPIResponseOK &&
          < BrowserRouter >
            <header className="App-header">
              <Headers />
            </header>
            <div className="appContainer">
              {!this.state.isPageComplete && <Loader />}
              <Switch>
                <Route to="/details" component={AllDetails}>

                </Route>
              </Switch>
              {this.state.isLoaded ?
                <>
                  <Slider products={this.state.productList} />
                </>
                : null
              }
              {this.state.isCategoryLoaded ?
                <>

                  <ProductCards products={this.state.categoryList} />
                </>
                : null
              }
            </div>
            <Footers />
            <FootersSecondary />
          </BrowserRouter>
        }
      </div>
    );
  };
};